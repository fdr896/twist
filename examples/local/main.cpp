#include <twist/rt/run.hpp>

#include <twist/ed/stdlike/thread.hpp>
#include <twist/ed/local/ptr.hpp>

#include <fmt/core.h>
#include <fmt/std.h>

// Each thread has its own (thread-local) copy of `ctx` pointer
twist::ed::ThreadLocalPtr<int> ctx;

void Baz() {
  fmt::println("Thread {}: ctx -> {}",
               twist::ed::stdlike::this_thread::get_id(),
               *ctx);
}

void Bar() {
  Baz();
}

void Foo() {
  Bar();
}

int main() {
  twist::rt::Run([] {
    int x = 1;
    ctx = &x;

    twist::ed::stdlike::thread t([] {
      int y = 2;
      ctx = &y;
      Foo();
    });

    t.join();

    Foo();
  });

  return 0;
}
