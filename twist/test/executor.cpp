#include <twist/test/executor.hpp>

#include <twist/rt/layer/fault/adversary/adversary.hpp>

#include <twist/rt/run/env/this.hpp>

namespace twist::test {

namespace detail {

void RunThreadRoutine(ThreadRoutine routine) {
#if defined(TWIST_FAULTY) && !defined(TWIST_FIBERS)
  SetThreadAffinity();
#endif

#if defined(TWIST_FAULTY)
  rt::fault::GetAdversary()->Enter();
#endif

  try {
    routine();
  } catch (...) {
    rt::this_env->Exception();
  }

#if defined(TWIST_FAULTY)
  rt::fault::GetAdversary()->Exit();
#endif
}

}  // namespace detail

void Executor::Submit(ThreadRoutine routine) {
  threads_.emplace_back([routine = std::move(routine)]() mutable {
    detail::RunThreadRoutine(std::move(routine));
  });
}

void Executor::Join() {
  if (joined_) {
    return;
  }
  for (auto& t : threads_) {
    t.join();
  }
  threads_.clear();
  joined_ = true;
}

////////////////////////////////////////////////////////////////////////////////

rt::strand::stdlike::thread RunThread(ThreadRoutine routine) {
  return rt::strand::stdlike::thread{[routine = std::move(routine)]() mutable {
    detail::RunThreadRoutine(std::move(routine));
  }};
}

}  // namespace twist::test
