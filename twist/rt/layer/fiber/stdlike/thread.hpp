#pragma once

#include <twist/rt/layer/fiber/runtime/fwd.hpp>
#include <twist/rt/layer/fiber/runtime/routine.hpp>
#include <twist/rt/layer/fiber/runtime/watcher.hpp>
#include <twist/rt/layer/fiber/runtime/wait_queue.hpp>

namespace twist::rt::fiber {

class ThreadLike : private IFiberWatcher {
  // Running [ -> Completed ] -> Detached
 public:
  ThreadLike(FiberRoutine routine);
  ~ThreadLike();

  // Non-copyable
  ThreadLike(const ThreadLike&) = delete;
  ThreadLike& operator=(const ThreadLike&) = delete;

  // Movable
  ThreadLike(ThreadLike&&);
  ThreadLike& operator =(ThreadLike&&);

  bool joinable() const;  // NOLINT
  void join();  // NOLINT
  void detach();  // NOLINT

  static unsigned int hardware_concurrency() noexcept;  // NOLINT

 private:
  bool IsDetached() const;
  bool IsCompleted() const;
  bool IsRunning() const;

  void Reset();
  void MoveFrom(ThreadLike&&);

  // IFiberWatcher
  void Completed() noexcept override;

 private:
  Fiber* fiber_;
  WaitQueue join_{"thread::join"};
};

}  // namespace twist::rt::fiber
