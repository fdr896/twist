#pragma once

#include <twist/rt/layer/fiber/runtime/fiber.hpp>
#include <twist/rt/layer/fiber/runtime/time.hpp>

#include <queue>
#include <tuple>
#include <chrono>

namespace twist::rt::fiber {

class SleepQueue {
 private:
  using TimePoint = VirtualTime::time_point;

 public:
  void Put(Fiber* sleeper, TimePoint deadline);

  bool IsEmpty() const;
  Fiber* Poll(TimePoint now);
  TimePoint NextDeadLine() const;

 private:
  struct Entry {
    Fiber* fiber;
    TimePoint deadline;

    using OrderingKey = std::tuple<TimePoint, FiberId>;

    OrderingKey Key() const {
      return {deadline, fiber->Id()};
    }

    bool operator<(const Entry& other) const {
      return Key() > other.Key();
    }
  };

 private:
  std::priority_queue<Entry> sleepers_;
};

}  // namespace twist::rt::fiber
