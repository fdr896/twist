#pragma once

#include <thread>

namespace twist::rt::thread {

namespace cores::single {

class [[nodiscard]] SpinWait {
 public:
  void SpinOnce() {
    std::this_thread::yield();
  }

  void operator()() {
    SpinOnce();
  }
};

}  // namespace cores::single

}  // namespace twist::rt::thread
