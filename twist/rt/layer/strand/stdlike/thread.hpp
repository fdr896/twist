#pragma once

#if defined(TWIST_FIBERS)

// cooperative user-space fibers

#include <twist/rt/layer/fiber/runtime/syscalls.hpp>
#include <twist/rt/layer/fiber/stdlike/thread.hpp>

#include <chrono>

namespace twist::rt::strand::stdlike {

using thread = fiber::ThreadLike;  // NOLINT

using thread_id = fiber::FiberId;  // NOLINT

extern const thread_id kInvalidThreadId;

namespace this_thread {

inline thread_id get_id() {  // NOLINT
  return fiber::GetId();
}

// scheduling

inline void yield() {  // NOLINT
  fiber::Yield();
}

inline void sleep_for(std::chrono::milliseconds delay) {  // NOLINT
  fiber::SleepFor(delay);
}

}  // namespace this_thread

}  // namespace twist::rt::strand::stdlike

#else

// native threads

#include <thread>

namespace twist::rt::strand::stdlike {

using ::std::thread;

using thread_id = ::std::thread::id;  // NOLINT

extern const thread_id kInvalidThreadId;

namespace this_thread = ::std::this_thread;

}  // namespace twist::rt::strand::stdlike

#endif
