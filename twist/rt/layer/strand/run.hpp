#pragma once

#if defined(TWIST_FIBERS)

#include <twist/rt/layer/fiber/runtime/scheduler.hpp>

namespace twist::rt::strand {

template <typename F>
void Run(F f) {
  twist::rt::fiber::Scheduler scheduler;
  scheduler.Run([f = std::move(f)] { f(); });
}

}  // namespace twist::rt::strand

#else

namespace twist::rt::strand {

template <typename F>
void Run(F f) {
  f();
}

}  // namespace twist::rt::strand

#endif
