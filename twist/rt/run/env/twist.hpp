#pragma once

#include <twist/rt/run/params.hpp>
#include <twist/rt/run/env.hpp>

#include <wheels/core/panic.hpp>
#include <wheels/core/exception.hpp>

#include <fmt/core.h>

#include <iostream>

namespace twist::rt {

// Standalone Twist test environment

class TwistEnv : public IEnv {
 public:
  TwistEnv(Params params = Params{})
      : params_(params) {
  }

  size_t Seed() const override {
    return params_.seed;
  }

  bool KeepRunning() const override {
    return true;
  }

  void PrintLine(std::string message) override {
    std::cout << message << std::endl;
  }

  void Exception() override {
    wheels::Panic(wheels::Here(), fmt::format("Unhandled exception: {}", wheels::CurrentExceptionMessage()));
  }

  void Assert(wheels::SourceLocation where, std::string reason) override {
    wheels::Panic(where, reason);
  }

 private:
  const Params params_;
};

}  // namespace twist::rt
