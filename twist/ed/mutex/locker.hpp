#pragma once

#include <utility>

namespace twist::ed {

// Replacement for std::unique_lock
// Does not write to memory after unlocking mutex

template <typename Mutex>
class Locker {
 public:
  explicit Locker(Mutex& mutex)
      : mutex_(&mutex) {
    mutex_->lock();
  }

  // Movable

  Locker(Locker&& that)
      : mutex_(that.Release()) {
  }

  Locker& operator=(Locker&& that) {
    AutoUnlock();
    mutex_ = that.Release();
    return *this;
  }

  // Non-copyable

  Locker(const Locker&) = delete;
  Locker& operator=(const Locker&) = delete;

  void Unlock() {
    ManualUnlock();
  }

  // NOLINTNEXTLINE
  void unlock() {
    Unlock();
  }

  ~Locker() {
    AutoUnlock();
  }

 private:
  Mutex* Release() {
    return std::exchange(mutex_, nullptr);
  }

  void ManualUnlock() {
    Mutex* mutex = Release();
    assert(mutex != nullptr);
    mutex->unlock();
  }

  void AutoUnlock() {
    if (Mutex* mutex = Release(); mutex != nullptr) {
      mutex->unlock();
    }
  }

 private:
  Mutex* mutex_;
};

}  // namespace twist::ed
