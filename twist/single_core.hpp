#pragma once

#if defined(TWIST_FIBERS) || PROC_COUNT == 1 || (defined(TWIST_FAULTY) && defined(LINUX))

#define TWIST_SINGLE_CORE 1

#else

#define TWIST_SINGLE_CORE 0

#endif
