#include <twist/fault/wrappers/atomic.hpp>

#include <wheels/support/compiler.hpp>
#include <wheels/test/test_framework.hpp>

TEST_SUITE(FlakyAtomic) {
  SIMPLE_TEST(DefaultCtor) {
    twist::fault::FaultyAtomic<int> var;
    WHEELS_UNUSED(var);
  }

  SIMPLE_TEST(StoreLoad) {
    twist::fault::FaultyAtomic<bool> var{false};
    ASSERT_FALSE(var.load());
    var.store(true);
    ASSERT_TRUE(var.load());
    var.store(false);
    ASSERT_FALSE(var.load());
  }

  SIMPLE_TEST(ReadModifyWrite) {
    twist::fault::FaultyAtomic<size_t> var{0};

    ASSERT_EQ(var.fetch_add(1), 0);
    ASSERT_EQ(var.load(), 1);

    ASSERT_EQ(var.fetch_add(3), 1);
    ASSERT_EQ(var.load(), 4);

    ASSERT_EQ(var.fetch_sub(2), 4);
    ASSERT_EQ(var.load(), 2);

    ASSERT_EQ(var.exchange(3), 2);
    ASSERT_EQ(var.load(), 3);

    {
      size_t expected = 2;
      ASSERT_FALSE(var.compare_exchange_strong(expected, 5));
    }

    {
      size_t expected = 3;
      ASSERT_TRUE(var.compare_exchange_strong(expected, 7));
      ASSERT_EQ(var.load(), 7);
    }
  }

  SIMPLE_TEST(Operators) {
    twist::fault::FaultyAtomic<int> var;

    var = 1;

    int non_atomic_var = var;
    ASSERT_EQ(non_atomic_var, 1);

    ASSERT_EQ((var = 2), 2);
    ASSERT_EQ(var, 2);
  }

  SIMPLE_TEST(FetchAdd) {
    twist::fault::FaultyAtomic<int> var;

    var = 1;
    ASSERT_EQ(var.fetch_add(2), 1);
    ASSERT_EQ(var.fetch_add(-1), 3);
    ASSERT_EQ(var.load(), 2);
  }

  SIMPLE_TEST(WaitNotify) {
    twist::fault::FaultyAtomic<std::uint32_t> var{1};

    var.wait(0); // Returns immediately
    ASSERT_EQ(var, 1);

    var.notify_one(); // Do nothing
    var.notify_all(); // Do nothing
  }

  SIMPLE_TEST(MemoryOrder) {
    twist::fault::FaultyAtomic<unsigned> var;

    // store and load

    var.store(40, std::memory_order_release);
    ASSERT_EQ(var.load(std::memory_order_acquire), 40);

    // wait
    var.wait(0, std::memory_order_acquire);

    // fetch add / sub

    ASSERT_EQ(var.fetch_add(3, std::memory_order_relaxed), 40);
    ASSERT_EQ(var.fetch_sub(1, std::memory_order_relaxed), 43);

    // compare_exchange_strong

    {
      unsigned expected = 42;
      ASSERT_TRUE(var.compare_exchange_strong(expected, expected + 1,
          std::memory_order_seq_cst));
    }

    {
      unsigned expected = 43;
      ASSERT_TRUE(var.compare_exchange_strong(expected, expected + 1,
          std::memory_order_seq_cst, std::memory_order_relaxed));
    }

    // compare_exchange_weak

    {
      unsigned expected = 44;
      var.compare_exchange_weak(expected, expected + 1,
          std::memory_order_seq_cst);
    }

    {
      unsigned expected = 45;
      var.compare_exchange_weak(expected, expected + 1,
          std::memory_order_seq_cst, std::memory_order_relaxed);
    }
  }
}
