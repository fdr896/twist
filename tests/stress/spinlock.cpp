#include <twist/test/with/wheels/stress.hpp>

#include <twist/ed/spin/lock.hpp>

#include <twist/test/race.hpp>
#include <twist/test/plate.hpp>
#include <twist/test/budget.hpp>

TEST_SUITE(SpinLock) {
  TWIST_TEST_REPEAT(Works, 5s) {
    twist::test::Plate plate;
    twist::ed::SpinLock spinlock;

    twist::test::Race race;

    for (size_t i = 0; i < 5; ++i) {
      race.Add([&] {
        while (twist::test::KeepRunning()) {
          std::lock_guard guard(spinlock);
          plate.Access();
        }
      });
    }

    race.Run();
  }
}
