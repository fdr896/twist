# General

ProjectLog("CMake version: ${CMAKE_VERSION}")
ProjectLog("C++ compiler: ${CMAKE_CXX_COMPILER}")
ProjectLog("C++ standard: ${CMAKE_CXX_STANDARD}")

# Twist-ed

if(TWIST_FAULTY)
    ProjectLog("Fault injection enabled")
endif()

if(TWIST_FIBERS)
    ProjectLog("Execution backend: Fibers")
else()
    ProjectLog("Execution backend: Threads")
endif()
